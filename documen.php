<?php

chdir(dirname(__FILE__));
require "../../autoload.php";

use \Documen\Documen;

/**
 * CLI for documen
 */
//var_dump($argv);
$command = $argv[1];
switch ($command) {
	case 'generate-docs': {
		$rootDir = dirname(dirname(dirname(dirname(__FILE__))));

		// Look for a default config file
		$configFile = $rootDir . '/documen.json';
		if (!file_exists($configFile)) {
			// No configuration file
			echo "Error: Missing documen.json file.\n\n";
			exit();
		}
		
		$config = json_decode(file_get_contents($configFile), true);
		if ($config == null) {
			// Bad json
			echo "Error: Invalid documen.json file. Could not parse.\n\n";
			exit();
		}
		
		if (!empty($config['output-dir'])) {
			$outputDir = $rootDir . $config['output-dir'];
		}
		else {
			// No output_dir
			echo "Error: Missing output_dir in documen.json file.\n\n";
			exit();
		}
		
		$config['root-dir'] = $rootDir;

		$d = new Documen();
		$d->setConfig($config);
		
		// TODO: Fix
		//$d->exportTo($config['output-dir']);
		$d->exportTo($outputDir);

		break;
	}
	default: {
		echo "Error: Bad command.\n\n";
		exit();
	}
}

echo "Finished!\n\n";