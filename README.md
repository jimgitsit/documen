**Overview**

Documen produces html documentation from any php [docbock](http://en.wikipedia.org/wiki/PHPDoc) including [markup](http://daringfireball.net/projects/markdown/syntax) within the docblock. You can tell it to read the docblock on anything from a global function to a class to an entire file.

**Install with composer**
```
"require": {
  "jimgitsit/documen": "dev-master"
}
```
**Usage**
In the root of your PHP applicaiton create a file called documen.json.
Here is where you will define what files, classes, methods, and function you want to generate documentation for.
Here is an example documen.json file:
```
{
  "output-dir": "/app/views/docs" <required, html output directory (relative to the vendor parent dir)>
  "css-file": "/public/css/document.css" <optional, custom css file>
  "classes": ["MyFancyClass"],
  "methods": [
    {
      "class": "MyFancyClass",
      "method": "fancyMethod"
    }
  ],
  "files": ["my-fancy-class.php"],
  "function": ["some_global_func"]
}
```

In terminal in project root run:
```
php vendor/jimgitsit/documen/documen.php generate-docs
```