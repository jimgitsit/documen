<?php

namespace Documen;

use \phpDocumentor\Reflection\DocBlock;

/**
 * 
 */
class Documen {
	
	private $config;
	
	public function setConfig($config) {
		$this->config = $config;
	}
	
	public function exportTo($path) {

		// TODO: Allow overriding the css file
		if (!empty($this->config['css-file'])) {
			// Custom css file
			$css = file_get_contents($this->config['root-dir'] . $this->config['css-file']);
		}
		else {
			// Default css file
			$css = file_get_contents(dirname(dirname(__FILE__)) . '/documen.css');
		}
		
		if (empty($this->config['html-wrap']) || $this->config['html-wrap'] == true) {
			$html = '<!DOCTYPE html><html><head lang="en"><meta charset="UTF-8"><title>' . $this->config['project-title'] . '</title><style>' . $css . '</style></head><body><div class="container">';
		}
		else {
			$html = '<div class="container">';
		}
		
		if (isset($this->config['project-title'])) {
			$html .= '<h2 class="project-title">' . $this->config['project-title'] . '</h2>';
		}
		
		if (isset($this->config['project-description'])) {
			$html .= '<p class="project-description">' . $this->config['project-description']. '</p>';
		}
		
		if (isset($this->config['methods'])) {
			foreach ($this->config['methods'] as $method) {
				$rc = new \ReflectionClass($method['class']);
				$rCmethod = $rc->getMethod($method['name']);
				$label = null;
				if (isset($method['label'])) {
					$label = $method['label'];
				}
				$html .= $this->getFunctionHTML($rc, $rCmethod, $label);
			}
		}
		
		if (isset($this->config['classes'])) {
			foreach ($this->config['classes'] as $className) {
				//$html .= '<div class="outside-class-name">' . $className . '</div>';
				$rc = new \ReflectionClass($className);
				$pubMethods = $rc->getMethods(\ReflectionMethod::IS_PUBLIC);
				
				if (isset($this->config['sort-methods']) && $this->config['sort-methods'] == true) {
					usort($pubMethods, array(self::class, 'sortMethods'));
				}
				
				foreach ($pubMethods as $method) {
					$html .= $this->getFunctionHTML($rc, $method, $method->name);
				}
			}
		}
		
		if (!empty($this->config['html-wrap']) && $this->config['html-wrap'] == true) {
			$html .= '</div></body></html>';
		}
		else {
			$html .= '</div>';
		}
		
		if (!is_dir($path)) {
			mkdir($path);
		}
		file_put_contents($path . '/index.html', $html);
	}
	
	private function sortMethods(\ReflectionMethod $a, \ReflectionMethod $b) {
		return strcmp($a->getName(), $b->getName());
	}
	
	private function getFunctionHTML($reflectionClass, $method, $label = null) {
		$phpdoc = new DocBlock($reflectionClass->getMethod($method->name));
		
		$tags = $phpdoc->getTagsByName('documen');
		$directives = array();
		foreach ($tags as $tag) {
			$directives = array_merge($directives, explode(',', $tag->getContent()));
		}
		
		if (in_array('nodoc', $directives)) {
			return '';
		}
		
		$shortDesc = $phpdoc->getShortDescription();
		$methodDesc = $phpdoc->getLongDescription();
		$formatted = $methodDesc->getFormattedContents();

		$html = '';
		if (isset($label)) {
			$html .= '<div class="method-wrap"><div class="custom-label">' . $label . '</div>';
		}
		else {
			$html .= '<div class="name"><div class="class-name">' . $method->class . '</div>::<div class="method-name">' . $method->name . '</div></div>';
		}
		$html .= '<div class="short-desc">' . $shortDesc . '</div>';
		$html .= '<div class="long-desc">' . $formatted . '</div>';
		
		return $html .= '</div>';
	}
}